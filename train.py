""" This script is used to train networks. It can be used to train on one or
several gpus
"""
import create_model as cm
import load_dataset as ld
from keras.utils import multi_gpu_model
from keras.metrics import top_k_categorical_accuracy
import numpy as np
from pathlib import Path
from keras.callbacks import CSVLogger
import argparse


def get_arg():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--size", type=int,
                        help='Minimal number of images in a taxon required for \
                              it to be selected for training',
                        default=500)
    default_path = str(Path('..', 'Dataset', 'Kaggle_Current',
                            'Dataset_Plankton'))
    parser.add_argument("-p", "--path", type=str, help='Path to dataset',
                        default=default_path)
    parser.add_argument("-c", "--csv", type=str, help='Folder containing csv',
                        default='csv')
    parser.add_argument('-g', '--gpus', type=int, nargs='+',
                        help='List of gpu id to use for the training',
                        default=[0])
    parser.add_argument('-e', '--epochs', type=int,
                        help='Number of epochs for the training',
                        default=20)
    parser.add_argument('-bs', '--batch_size', type=int,
                        help='Batch size for the training',
                        default=64)
    args = parser.parse_args()
    print(args)
    return(args.size, args.path, args.csv, args.gpus, args.epochs,
           args.batch_size)


def top3(y_true, y_pred):
    return(top_k_categorical_accuracy(y_true, y_pred, k=3))

if __name__ == '__main__':
    min_size, dataset_path, csv_path, gpu_list, e, bs = get_arg()
    # Get data (image + metadata) for the training
    inputs_train, inputs_test, labels_train, labels_test, class_weights, \
        = ld.get_data(min_size, dataset_path, csv_path)
    nb_class = labels_test.shape[1]
    nb_feature = inputs_test[1].shape[1]
    # Create and compile model
    model = cm.create_model(nb_feature, nb_class)
    csv_logger = CSVLogger(f'csv/log_f{nb_feature}_c{nb_class}.csv')
    if len(gpu_list) > 1:
        multi_gpu = multi_gpu_model(model, gpus=gpu_list)
        multi_gpu.compile(loss='categorical_crossentropy',
                          optimizer='adadelta',  # optimizer='rmsprop'
                          metrics=['accuracy', top3])
        # Train model
        multi_gpu.fit(x=inputs_train, y=labels_train, epochs=e,
                      batch_size=bs,  # 4096
                      validation_data=(inputs_test, labels_test),
                      class_weight=class_weights,
                      callbacks=[csv_logger])
    else:
        model.compile(loss='categorical_crossentropy',
                      optimizer='adadelta',  # optimizer='rmsprop'
                      metrics=['accuracy', top3])
        model.fit(x=inputs_train, y=labels_train, epochs=e,
                  batch_size=bs,  # 4096
                  validation_data=(inputs_test, labels_test),
                  class_weight=class_weights,
                  callbacks=[csv_logger])
    # Save model
    Path('models/').mkdir(exist_ok=True)
    # Save model via the template model (which shares the same weights)
    model.save(f'models/my_model_f{nb_feature}_c{nb_class}_bs{bs}_e{e}.h5')
