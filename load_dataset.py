""" This module is meaned to centralize all the method to load data
into memory. For training, testing and visualisation purpose
"""

import numpy as np
import cv2
import pandas as pd
from tqdm import tqdm
from sklearn import preprocessing
from pathlib import Path
from sklearn.utils import class_weight
from sklearn.preprocessing import RobustScaler
from sklearn.model_selection import train_test_split
from joblib import dump, load


def load_csv(csv_path):
    """ This function is used to load the csv
    The commented part shoud be used to clean the
    """
    metada_csv_path = str(Path(csv_path, 'complete_csv.csv'))
    metadata_csv = pd.read_csv(metada_csv_path)
    metadata_csv.drop(['TimeOfFlight', 'Date', 'Area', 'ESD', 'Biovolume',
                       'FeretDiameter', 'MajorAxisLength', 'MinorAxisLength'],
                      axis=1, inplace=True)
    """
    input_csv_path = str(Path(csv_path, 'complete_csv.csv'))
    print("Loading csv and cleaning it")
    
    size = len(metadata_csv.index)
    input_csv = pd.read_csv(input_csv_path, index_col=0)
    already_dropped = []
    with tqdm(total=len(input_csv.columns)) as pbar:
        for name in input_csv:
            try:
                def remove_irrelevant(x, min_range=input_csv[name]['min'], max_range=input_csv[name]['max']):
                    if x <= min_range or x >= max_range:
                        return(np.nan)
                    else:
                        return(x)
                metadata_csv[name] = metadata_csv[name].apply(remove_irrelevant)
            except KeyError:
                already_dropped.append(name)
                pass
            pbar.update(1)
    print(f'Some metadata have been ignored (dropped before): {" ".join(str(x) for x in already_dropped)}')
    metadata_csv.dropna(inplace=True)
    print(f"Dropped {round((1 - len(metadata_csv.index)/size)*100,2)} % of the dataset")
    """
    return(metadata_csv)


def remove_small_taxon(metadata_csv, min_size):
    print(f'Dropping taxon  that have less than {min_size} samples')
    list_array_dropped = []
    for i in metadata_csv['Category_Taxon'].value_counts().iteritems():
        if i[1] < min_size:
            list_array_dropped.append(i[0])
            metadata_csv = metadata_csv[metadata_csv['Category_Taxon'] != i[0]]
    print(f'Drop : {" ".join(str(x) for x in list_array_dropped)}')
    metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] != 'BQL']
    metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] != 'Other']
    metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] != 'Not-living']
    metadata_csv = metadata_csv[metadata_csv['Category_Taxon'] != 'Multiple']
    metadata_csv = metadata_csv[metadata_csv['Category_Taxon'] != 'Clumps']
    metadata_csv = metadata_csv[metadata_csv['Category_Taxon'] != 'Bad-focus']
    return(metadata_csv)


# Maybe some image more large than long????
def load_images(metadata_csv, dataset_path, size_image_x=100, size_image_y=48):
    print("Load images in memory")
    number_images = len(metadata_csv.index)
    one_percent_images = number_images // 100
    imgs = np.empty((number_images, size_image_y, size_image_x), dtype='uint8')
    with tqdm(total=number_images) as pbar:
        for num, row in enumerate(metadata_csv.itertuples()):
            if num % one_percent_images == 0 and num != 0:
                pbar.update(one_percent_images)
            prepath = dataset_path
            path = Path(prepath, row.Category_Taxon, str(row.FolderNumber).zfill(5), row.ImgName + '.png')
            # print(cv2.imread(str(path), cv2.IMREAD_GRAYSCALE).shape)
            # print(cv2.resize(cv2.imread(str(path), cv2.IMREAD_GRAYSCALE), (size_image_x, size_image_y)).shape)
            imgs[num, :, :] = cv2.resize(cv2.imread(str(path), cv2.IMREAD_GRAYSCALE), (size_image_x, size_image_y))
    return imgs


def get_labels(vector_taxon):
    enc = preprocessing.OneHotEncoder()
    enc.fit(vector_taxon[:, np.newaxis])
    category_vec = enc.transform(vector_taxon[:, np.newaxis])
    labels_unique = np.unique(vector_taxon)
    dump(enc, f'obj/Encoder{labels_unique.size}.joblib')
    print(f'There is {labels_unique.size} taxons selected for the model')
    dump(labels_unique, f'obj/labels_unique_{labels_unique.size}.joblib')
    class_weights = class_weight.compute_class_weight('balanced',
                                                      labels_unique,
                                                      vector_taxon)
    return(category_vec, class_weights)


# Function for image normalisation and data augmentation, not used yet
def generator_imgs_meta(split):
    imgs_train, imgs_test, metadata_values_train, metadata_values_test, labels_train, labels_test = split

    # Standarize metadata
    transformer = RobustScaler().fit(metadata_values_train)
    metadata_values_train = transformer.transform(metadata_values_train)
    metadata_values_test = transformer.transform(metadata_values_test)

    # create generator to standardize images
    datagen = ImageDataGenerator(featurewise_center=True,
                                 featurewise_std_normalization=True)
    # calculate mean on training dataset
    datagen.fit(imgs_train)
    genX1 = gen.flow(imgs_train, metadata_values_train,  batch_size=batch_size, seed=1)
    genX2 = gen.flow(imgs_train, labels_train, batch_size=batch_size, seed=1)
    genX3 = gen.flow(imgs_test, metadata_values_test,  batch_size=batch_size, seed=1)
    genX4 = gen.flow(imgs_test, labels_test, batch_size=batch_size, seed=1)
    while True:
        X1 = genX1.next()
        X2 = genX2.next()
        X3 = genX1.next()
        X4 = genX2.next()
        yield [X1[0], X1[1]], X2[1]


def get_data(min_size, dataset_path, csv_path):
    # Loading the csv and drop some category that are not going to be used
    metadata_csv = load_csv(csv_path)
    # remove taxon too small
    metadata_csv = remove_small_taxon(metadata_csv, min_size)
    # Loading images from the csv
    imgs = load_images(metadata_csv, dataset_path)[:, :, :, np.newaxis]
    # Convert the name of labels into vector for the machine learning + save group
    vector_taxon = metadata_csv['Category_Taxon'].to_numpy()
    labels, class_weights = get_labels(vector_taxon)
    # Drop the feature that are not going to be used in the training
    metadata_csv.drop(['ImgName', 'Category_Taxon', 'Category_Grouped',
                       'NumberOfROIinTrigger', 'FolderNumber'],
                      axis=1, inplace=True)
    metadata_values = metadata_csv.values
    # Split the dataset with a random state to ensure the consistency
    # over several run. Use stratify to ensure that the proportion of
    # taxon is the same in the training and testing set. vector_taxon
    # is used instead of labels, because labels is a sparce matrix
    split = train_test_split(imgs, metadata_values, labels,
                             train_size=0.7, random_state=42,
                             stratify=vector_taxon)
    imgs_train, imgs_test, metadata_values_train, metadata_values_test, \
        labels_train, labels_test = split
    inputs_train = [imgs_train, metadata_values_train]
    inputs_test = [imgs_test, metadata_values_test]
    # Normalisation of metadata
    transformer = RobustScaler().fit(metadata_values_train)
    metadata_values_train = transformer.transform(metadata_values_train)
    metadata_values_test = transformer.transform(metadata_values_test)
    return(inputs_train, inputs_test, labels_train, labels_test, class_weights)


if __name__ == '__main__':
    get_data(50000,
             str(Path('..', 'Dataset', 'Kaggle_Current', 'Dataset_Plankton')),
             'csv'
             )
