""" This script is intend to generate mosaic of images using a
stucture define into a csv, in order to facilitate the visualisation
of the classification/cluster created
"""

from rectpack import newPacker
import cv2
import numpy as np
from pathlib import Path
import argparse
import pandas as pd


def get_arg():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--size", type=int, help='size mosaic',
                        default=2000)
    # default_path = str(Path('..', 'hdbscan', 'pca10_other'))
    default_path = str(Path('..', 'supervised'))
    parser.add_argument("-p", "--path", type=str,
                        help='path to image to merge',
                        default=default_path)
    args = parser.parse_args()
    print(args)
    return(args.size, args.path)


def create_mosaic(subdirectory, size, display=False):
    print('Loading Images')
    imgs_list = subdirectory.rglob('*.png')
    img = []
    rectangles = []
    for i, p in enumerate(imgs_list):
        new_image = cv2.imread(str(p), cv2.IMREAD_GRAYSCALE)
        img.append(new_image)
        rectangles.append(np.append(new_image.shape, i))
    img = np.array(img)

    bins = [(size, size)]

    packer = newPacker(rotation=False)

    # Add the rectangles to packing queue
    for r in rectangles:
        packer.add_rect(*r)

    # Add the bins where the rectangles will be placed
    for b in bins:
        packer.add_bin(*b, count=float("inf"))

    # Start packing
    packer.pack()

    # Obtain number of bins used for packing
    nbins = len(packer)

    # Number of rectangles packed into first bin
    nrect = len(packer[0])

    # Full rectangle list
    all_rects = packer.rect_list()

    # b - Bin index
    # x - Rectangle bottom-left corner x coordinate
    # y - Rectangle bottom-left corner y coordinate
    # w - Rectangle width
    # h - Rectangle height
    # rid - User asigned rectangle id or None
    print('Creating mosaics')
    dim = tuple([nbins]) + bins[0]
    print(f'Images stored in {dim[0]} mosaics of size {dim[1:]}')
    save_img = np.zeros(shape=dim).astype(np.uint8)

    for rect in all_rects:
        b, x, y, w, h, rid = rect
        try:
            save_img[b, x:x+w, y:y+h] = img[rid]
        except ValueError:
            save_img[b, x:x+w, y:y+h] = np.transpose(img[rid])

    for i in range(nbins):
        if display:
            cv2.imshow('image', save_img[i, :, :])
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        name = f'supervised/{subdirectory.stem}_{i}.png'
        cv2.imwrite(name, save_img[i, :, :])


def create_mosaic2(subdirectory, size, df_cluster, display=False):
    print('Loading Images')
    prepath = str(Path('..', 'Dataset', 'Kaggle_Current',
                       'Dataset_Plankton'))
    img = []
    rectangles = []

    for num, row in enumerate(df_cluster.itertuples()):
            path = Path(prepath, row.Category_Taxon,
                        str(row.FolderNumber).zfill(5), row.ImgName + '.png')
            new_image = cv2.imread(str(path), cv2.IMREAD_GRAYSCALE)
            img.append(new_image)
            rectangles.append(np.append(new_image.shape, num))
    img = np.array(img)
    bins = [(size, size)]

    packer = newPacker(rotation=False)

    # Add the rectangles to packing queue
    for r in rectangles:
        packer.add_rect(*r)

    # Add the bins where the rectangles will be placed
    for b in bins:
        packer.add_bin(*b, count=float("inf"))

    # Start packing
    packer.pack()

    # Obtain number of bins used for packing
    nbins = len(packer)

    # Number of rectangles packed into first bin
    nrect = len(packer[0])

    # Full rectangle list
    all_rects = packer.rect_list()

    # b - Bin index
    # x - Rectangle bottom-left corner x coordinate
    # y - Rectangle bottom-left corner y coordinate
    # w - Rectangle width
    # h - Rectangle height
    # rid - User asigned rectangle id or None
    print('Creating mosaics')
    dim = tuple([nbins]) + bins[0]
    print(f'Images stored in {dim[0]} mosaics of size {dim[1:]}')
    save_img = np.zeros(shape=dim).astype(np.uint8)

    for rect in all_rects:
        b, x, y, w, h, rid = rect
        try:
            save_img[b, x:x+w, y:y+h] = img[rid]
        except ValueError:
            save_img[b, x:x+w, y:y+h] = np.transpose(img[rid])

    for i in range(nbins):
        if display:
            cv2.imshow('image', save_img[i, :, :])
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        name = f'test/{subdirectory}_{i}.png'
        cv2.imwrite(name, save_img[i, :, :])

if __name__ == '__main__':
    size, img_path = get_arg()
    img_path = Path(img_path)
    Path('test').mkdir(exist_ok=True)
    df = pd.read_csv('csv/cluster_pca1_meth_leaf.csv')
    metadata = pd.read_csv('csv/complete_csv.csv')
    clusters = np.unique(df['cluster'])
    for cluster in clusters:
        if cluster != -1:
            df_cluster = df[df['cluster'] == cluster]
            df_cluster = pd.merge(df_cluster, metadata, on='ImgName',
                                  how='inner')
            create_mosaic2(cluster, size, df_cluster)
