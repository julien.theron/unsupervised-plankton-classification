import pandas as pd
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler


def train_random_forest(vector, label, name, df_rf):
    vector_train, vector_test, labels_train, labels_test = train_test_split(
        vector, label, train_size=0.7, random_state=42, stratify=label)

    clf2 = RandomForestClassifier(n_estimators=200,  n_jobs=-1)
    clf2.fit(vector_train, labels_train)
    unique, counts = np.unique(clf2.predict(vector_test), return_counts=True)
    random_forest_labels = clf2.predict(vector_test)
    base_prop_true_positive = round(np.sum(label)/label.shape[0], 3)
    base_nb = labels_test.shape[0]
    base_nb_true_positive = np.sum(labels_test)
    # score_rf = clf2.score(vector_test, labels_test)
    # Computation of the proportion of samples rightfully dropped
    rf_nb_positive = np.sum(random_forest_labels)
    rf_nb_true_positive = np.dot(random_forest_labels, labels_test)
    rf_nb_negative = np.count_nonzero(random_forest_labels == 0)
    rf_nb_true_negative = np.dot(random_forest_labels - 1, labels_test-1)
    rf_prop_true_positive = round(rf_nb_true_positive / rf_nb_positive, 3)
    nb_remove = np.count_nonzero(random_forest_labels == 0)
    rf_prop_true_negative = round(rf_nb_true_negative / nb_remove, 3)
    rf_prop_true_positive_keep = rf_nb_true_positive/base_nb_true_positive
    df_rf = df_rf.append({'Category_Taxon': name,
                          'BaseNb': base_nb,
                          'BaseNbTP': base_nb_true_positive,
                          'RfNbP': rf_nb_positive,
                          'RfNbTP': rf_nb_true_positive,
                          'RfNbN': rf_nb_negative,
                          'RfNbTN': rf_nb_true_negative,
                          'BPTP': base_prop_true_positive,
                          'RfPTP': rf_prop_true_positive,
                          'RfPTN': rf_prop_true_negative,
                          'RfPropTPKeep': rf_prop_true_positive_keep,
                          }, ignore_index=True)
    # repartition_rf = dict(zip(unique, counts))
    return(df_rf)

if __name__ == '__main__':
    predictions = pd.read_feather('csv/PredictedCluster_inter.f')
    fields = ['ImgName', 'Category_Taxon']
    metadata = pd.read_csv('csv/complete_csv.csv', usecols=fields)
    df_rf = pd.DataFrame(columns=['Category_Taxon',
                                  'BaseNb',
                                  'BaseNbTP',
                                  'RfNbP',
                                  'RfNbTP',
                                  'RfNbN',
                                  'RfNbTN',
                                  'BPTP',
                                  'RfPTP',
                                  'RfPTN',
                                  'RfPropTPKeep',
                                  ])
    for name in np.unique(predictions['PredictedCluster'].values):
        # print(name)
        df = predictions[predictions['PredictedCluster'] == name]
        nbimage_tot = len(metadata[metadata['Category_Taxon'] == name].index)
        df = metadata.merge(df, on='ImgName')
        label = df['Category_Taxon'].equals(df['PredictedCluster'])
        label = np.where(df['Category_Taxon'] == df['PredictedCluster'], 1, 0)
        vector = df.drop(['ImgName', 'Category_Taxon', 'PredictedCluster'],
                         axis=1).values
        # pca = make_pipeline(StandardScaler(),
        #                     PCA(n_components=2))
        # vector_2d = pca.fit(vector, label).transform(vector)
        df_rf = train_random_forest(vector, label, name, df_rf)
        print(df_rf.tail(1))
        # df = df.sort_values(by=['RfPTN'])
    # df = df.sort_values(by=['RfPTN'])
    df_rf.to_csv('csv/randomforest_inter.csv', index=False)
