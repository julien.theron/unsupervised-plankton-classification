from joblib import dump, load
from keras.models import load_model, Model
import load_dataset as ld
import train as tr
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from scipy import stats
from sklearn.cluster import KMeans, OPTICS
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import (LinearDiscriminantAnalysis,
                                           QuadraticDiscriminantAnalysis)
from sklearn import metrics
from sklearn.neighbors import (KNeighborsClassifier,
                               NeighborhoodComponentsAnalysis)
from sklearn.preprocessing import normalize, StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.utils.multiclass import unique_labels
import shutil
import argparse
from joblib import dump, load
import pandas as pd


def get_arg():
    parser = argparse.ArgumentParser()
    default_path = str(Path('..', 'Dataset', 'Kaggle_Current',
                            'Dataset_Plankton'))
    parser.add_argument("-p", "--path", type=str, help='path to dataset',
                        default=default_path)
    parser.add_argument("-c", "--csv", type=str, help='folder containing csv',
                        default='csv')
    args = parser.parse_args()
    print(args)
    return(args.path, args.csv)


def get_new_images(dataset_path, csv_path, personalize=None):
    metadata_csv = ld.load_csv(csv_path)
    if personalize:
        metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] == personalize]
    # Loading images from the csv
    imgs = ld.load_images(metadata_csv, dataset_path)[:, :, :, np.newaxis]
    # Convert the name of labels into vector for the machine learning + save group
    labels, class_weights, c = ld.get_labels(metadata_csv)
    nb_class = labels.shape[1]
    # Drop the feature that are not going to be used in the training
    metapath = metadata_csv[['ImgName', 'Category_Taxon', 'FolderNumber']]
    metadata_csv.drop(['ImgName', 'Category_Taxon', 'Category_Grouped',
                       'NumberOfROIinTrigger', 'FolderNumber'],
                      axis=1, inplace=True)
    metadata_values = metadata_csv.values
    return([imgs, metadata_values], labels, class_weights, nb_class, metapath)


if __name__ == '__main__':
    use_intermediate_output = True
    dataset_path, csv_path = get_arg()
    inputs, labels, class_weights, nb_class, metapath = \
        get_new_images(dataset_path, csv_path, personalize=None)
    dependencies = {'top3': tr.top3}
    model = load_model(f'models/my_model_f6_c42single.h5',
                       custom_objects=dependencies)
    print(model.summary())
    if use_intermediate_output:
        layer_name = 'dense_9'
        inter_model = Model(inputs=model.input,
                            outputs=model.get_layer(layer_name).output)
        inter_output = inter_model.predict(inputs, verbose=1, batch_size=4096)
    final_output = model.predict(inputs, verbose=1, batch_size=4096)
    dump(final_output, 'obj/trash.joblib')
    dump(metapath, 'obj/trash2.joblib')
    # output = load('obj/trash.joblib')
    # metapath = load('obj/trash2.joblib')
    enc = load(f'obj/Encoder42.joblib')
    print(f'Output is {final_output} and from size {final_output.shape}')
    name_labels = enc.inverse_transform(final_output)

    conc = np.concatenate(name_labels, axis=0)[:, np.newaxis]
    print(f'conc is {conc} and from size {conc.shape}')
    ImgName = metapath['ImgName'].values[:, np.newaxis]
    print(f'ImgName is {ImgName} with size {ImgName.shape}')
    columns_values = np.concatenate((ImgName, conc, final_output), axis=1)
    columns_names = ['ImgName', 'PredictedCluster'] + \
        [str(i) for i in range(final_output.shape[1])]
    df = pd.DataFrame(columns_values, columns=columns_names)
    df.to_feather('csv/PredictedCluster_final.f')
    if use_intermediate_output:
        columns_values = np.concatenate((ImgName, conc, inter_output), axis=1)
        columns_names = ['ImgName', 'PredictedCluster'] + \
            [str(i) for i in range(inter_output.shape[1])]
        df = pd.DataFrame(columns_values, columns=columns_names)
        df.to_feather('csv/PredictedCluster_inter.f')
    # df.to_csv('csv/PredictedCluster.csv', index=False)
    # df.to_hdf('csv/PredictedCluster.h5', key='df', mode='w')
