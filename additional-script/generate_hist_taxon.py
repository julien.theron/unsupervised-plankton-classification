""" Script use to generate histogram of specific group/Taxon """
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('csv/imagemeta.csv')
unique, counts = np.unique(df['Category_Taxon'], return_counts=True)
org = np.argsort(counts)[::-1]
counts = counts[org]
unique = unique[org]
dictionnary = dict(zip(unique, counts))
print(dictionnary)
width = 1.0
plt.figure(figsize=(18, 8))
plt.bar(dictionnary.keys(), dictionnary.values(), width, color='g', log=True)
plt.xticks(rotation=90)
# Tweak spacing to prevent clipping of tick-labels
plt.subplots_adjust(bottom=0.3)
plt.show()
