""" This script was used to generate a csv of all images in the subfolder
of a selected folder and to use the name of the subfolder as a label
"""

import pandas as pd
from pathlib import Path
from tqdm import tqdm
import numpy as np

if __name__ == '__main__':
    path_folder = '../hdbscan/pca10_other'
    format_image = 'png'
    p = Path(path_folder).rglob(f'*.{format_image}')
    p2 = Path(path_folder).rglob(f'*.{format_image}')
    number_images = sum(1 for _ in p2)
    one_percent_images = number_images // 100
    print(f'Found {number_images} {format_image} images in \
           the folder {path_folder}')
    # Allocating the numpy array before iterating
    ImgName = np.empty(number_images, dtype='<U4')
    cluster = np.ones(number_images, dtype=np.int8)
    with tqdm(total=number_images) as pbar:
        for i, path in enumerate(p):
            if i % one_percent_images == 0 and i != 0:
                pbar.update(one_percent_images)
            ImgName[i] = path.stem
            cluster[i] = int(path.parent.stem)
    ImgName = ImgName[:, np.newaxis]
    cluster = cluster[:, np.newaxis]
    concatenated_array = np.concatenate((ImgName, cluster), axis=1)
    df = pd.DataFrame(concatenated_array, columns=['ImgName', 'cluster'])
    df.to_csv(path_or_buf='csv/cluster.csv', index=False)
