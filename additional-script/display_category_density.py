""" This scipt was usefor for the plotting the images of the projection
in the 2D space of the features of predicted category. See README
"""

import pandas as pd
from sklearn.pipeline import make_pipeline
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

predictions = pd.read_feather('../csv/PredictedCluster_inter.f')
fields = ['ImgName', 'Category_Taxon']
metadata = pd.read_csv('../csv/complete_csv.csv', usecols=fields)
print(predictions.shape)
for name in np.unique(predictions['PredictedCluster'].values):
    print(name)
    df = predictions[predictions['PredictedCluster'] == name]
    df = df.merge(metadata, on='ImgName')
    label = (df['Category_Taxon'] == name) * 1
    print(label.shape)
    true_label = np.argwhere(df['Category_Taxon'] == name)
    false_label = np.argwhere(df['Category_Taxon'] != name)
    vector = df.drop(['ImgName', 'Category_Taxon', 'PredictedCluster'],
                     axis=1).values
    # print(vector.shape)
    pca = make_pipeline(StandardScaler(),
                        PCA(n_components=2))
    print(vector.shape)
    vector_2d = pca.fit(vector, label).transform(vector)
    print(vector_2d.shape)
    min_range = np.min(vector_2d, axis=0)
    max_range = np.max(vector_2d, axis=0)
    print(min_range)
    print(max_range)
    plt.figure(name, figsize=(18, 6), dpi=100)
    plt.subplot(131)
    plt.hist2d(vector_2d[:, 0].flatten(),
               vector_2d[:, 1].flatten(),
               range=([min_range[0], max_range[0]], [min_range[1], max_range[1]]),
               bins=(50, 50),
               cmap=plt.cm.jet)
    plt.title('Whole Category')
    plt.colorbar()
    plt.subplot(132)
    plt.hist2d(vector_2d[true_label, 0].flatten(),
               vector_2d[true_label, 1].flatten(),
               range=([min_range[0], max_range[0]], [min_range[1], max_range[1]]),
               bins=(50, 50),
               cmap=plt.cm.jet)
    plt.title('True Positive')
    plt.colorbar()
    # plt.scatter(vector_2d[true_label, 0], vector_2d[true_label, 1], marker='^', c='r')
    plt.subplot(133)
    plt.hist2d(vector_2d[false_label, 0].flatten(),
               vector_2d[false_label, 1].flatten(),
               range=([min_range[0], max_range[0]], [min_range[1], max_range[1]]),
               bins=(50, 50),
               cmap=plt.cm.jet)
    plt.title('False Positive')
    plt.colorbar()
    # plt.scatter(vector_2d[false_label, 0], vector_2d[false_label, 1], marker='o', c='b')
    plt.show()
