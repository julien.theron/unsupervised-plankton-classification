"""This script is now obsolete with the new version of the dataset
It was mean to add the size in both axis for all images and the folder number
they where in (to facilate their loading in memory). Those information are
now directly included in the csv
"""

import pandas as pd
from pathlib import Path
import numpy as np
from PIL import Image
from tqdm import tqdm


def get_info(path_folder='../Dataset', format_image='png'):
    p = Path(path_folder).rglob(f'*.{format_image}')
    # Load an other iterator to get the number of images in folders
    p2 = Path(path_folder).rglob(f'*.{format_image}')
    number_images = sum(1 for _ in p2)
    one_percent_images = number_images // 100
    print(f'Found {number_images} {format_image} images in the folder {path_folder}')
    # Allocating the numpy array before iterating
    ImgName = np.empty(number_images, dtype='<U4')
    image_size_x = np.empty(number_images)
    image_size_y = np.empty(number_images)
    folder_number = np.empty(number_images, dtype='U5')
    with tqdm(total=number_images) as pbar:
        for i, path in enumerate(p):
            if i % one_percent_images == 0 and i != 0:
                pbar.update(one_percent_images)
            ImgName[i] = path.stem
            image_size_x[i], image_size_y[i] = np.uint16(Image.open(path).size)  # Lazy opening image
            folder_number[i] = path.parent.stem
    # Add another axis that is going to be use to create the dataframe
    ImgName = ImgName[:, np.newaxis]
    image_size_x = image_size_x[:, np.newaxis]
    image_size_y = image_size_y[:, np.newaxis]
    folder_number = folder_number[:, np.newaxis]
    concatenated_array = np.concatenate((ImgName, image_size_x, image_size_y, folder_number), axis=1)
    df = pd.DataFrame(concatenated_array, columns=['ImgName', 'SizeX', 'SizeY', 'FolderNumber'])
    return(df)

if __name__ == '__main__':
    df = get_info()
    print('Reading metadata csv')
    csv = pd.read_csv('csv/imagemeta.csv')
    print('Merging metadata and csv')
    csv = pd.merge(csv, df, how='right', on=['ImgName'])
    csv_save_path = 'csv/complete_csv.csv'
    print(f'Saving the csv to {csv_save_path}')
    csv.to_csv(path_or_buf='csv/complete_csv.csv', index=False)
