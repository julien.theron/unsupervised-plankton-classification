""" This script is now obsolete. It was mean to find the images that has not
been generated (in an old version of the dataset there was some sample with
only metadata, no images)

"""

import pandas as pd
from pathlib import Path
import numpy as np
from tqdm import tqdm


path_folder = '../Dataset'
format_image = 'png'
p = Path(path_folder).rglob(f'*.{format_image}')
p2 = Path(path_folder).rglob(f'*.{format_image}')
number_images = sum(1 for _ in p2)
one_percent_images = number_images // 100
print(f'Found {number_images} {format_image} images in the folder {path_folder}')
# Allocating the numpy array before iterating
ImgName = np.empty(number_images, dtype='<U4')
In = np.ones(number_images, dtype=np.uint8)
with tqdm(total=number_images) as pbar:
    for i, path in enumerate(p):
        if i % one_percent_images == 0 and i != 0:
            pbar.update(one_percent_images)
        ImgName[i] = path.stem
ImgName = ImgName[:, np.newaxis]
In = In[:, np.newaxis]
concatenated_array = np.concatenate((ImgName, In), axis=1)
df = pd.DataFrame(concatenated_array, columns=['ImgName','In'])
csv = pd.read_csv('csv/imagemeta.csv')
csv = csv[['ImgName', 'Category_Grouped', 'Category_Taxon']]
csv_merge = pd.merge(csv, df, on='ImgName', how='outer')
csv_absent = csv_merge[csv_merge.isnull().any(axis=1)]
csv_absent = csv_absent.drop(['In'], axis=1)
csv_absent.to_csv(path_or_buf='csv/missing_images.csv', index=False)
print(csv_absent.shape)
print(csv_merge['In'].describe(percentiles=[0.001, 0.01, .16, .5, .84, 0.999]))
