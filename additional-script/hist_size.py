import pandas as pd
import matplotlib.pyplot as plt
import load_dataset as ld
from pathlib import Path
import cv2


def display_images(df):
    big_images = df[(df['SizeX'] > 500) | (df['SizeY'] > 200)]
    for num, row in enumerate(big_images.itertuples()):
        prepath = Path('..', 'Dataset', 'Kaggle_Current', 'Dataset_Plankton')
        path = Path(prepath, row.Category_Taxon, str(row.FolderNumber).zfill(5), row.ImgName + '.png')
        img = cv2.imread(str(path), cv2.IMREAD_GRAYSCALE)
        cv2.imshow(row.ImgName, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def get_info_size(df):
    df_Size = df[['SizeX', 'SizeY']]
    print(df_Size.describe(percentiles=[0.001, 0.01, .16, .5, .84, 0.9, 0.95, 0.99, 0.999]))
    ax = df_Size.hist(bins=100)
    plt.show()


def get_correlation():
    metadata_csv = pd.read_csv('csv/complete_csv.csv')
    print(metadata_csv[['SizeX', 'SizeY', 'MajorAxisLength', 'MinorAxisLength']].corr())
    print(metadata_csv['Category_Grouped'].value_counts())
    print(metadata_csv['Category_Taxon'].value_counts())

# df = ld.load_csv('csv/complete_csv.csv')
# get_info_size(df)
# display_images(df)

metadata_csv = ld.load_csv()
# print(metadata_csv[['NumberOfROIinTrigger']].describe())
for i in metadata_csv['Category_Grouped'].value_counts().iteritems():
    if i[1] < 1000:
        print(f'Drop {i[0]}')
        metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] != i[0]]
# plt.figure()
# for i in metadata_csv['Category_Taxon'].value_counts().iteritems():
    # print(i)
metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] != 'BQL']
metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] != 'Other']
metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] != 'Not-living']

grouped = metadata_csv['Category_Taxon'].value_counts()
grouped.plot.bar(log=True)
taxon = metadata_csv['Category_Taxon'].value_counts()  # transpose().hist(bins=100)
taxon.plot.bar(log=True)
metadata_csv[['SizeX', 'Category_Grouped']].boxplot(by='Category_Grouped', whis=[5, 95])
# metadata_csv[['SizeY', 'Category_Grouped']].boxplot(by='Category_Grouped', whis=[5, 95])
plt.show()
# metadata_csv['Category_Taxon'].value_counts().hist()
