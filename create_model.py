""" This module is intended to create the keras model to be trained.
The input and ouput can be choosed through parameters.
It can also be executed to generate an image of the model and display
the model's structure on the terminal
"""

from keras.models import Model
from keras.layers import Input, concatenate
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization
from keras.utils import plot_model
from pathlib import Path


def conv_block(x, factor):
    x = Conv2D(16*factor, (3, 3), padding="same", activation='relu')(x)
    x = BatchNormalization()(x)
    x = Conv2D(16*factor, (3, 3), padding="same", activation='relu')(x)
    x = BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(0.3)(x)
    return(x)


def create_model(nb_features, output_size):
    image_input = Input(shape=(48, 100, 1), dtype='float32', name='image_input')

    x = conv_block(image_input, 1)
    x = conv_block(x, 2)
    x = conv_block(x, 4)
    x = conv_block(x, 8)

    x = Flatten()(x)
    x = Dense(256, activation='relu')(x)
    x = Dense(256, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    cnn_out = Dropout(0.3)(x)

    metadata_input = Input(shape=(nb_features, ), name='metadata_input')
    y = Dense(16, activation='relu')(metadata_input)
    y = Dropout(0.3)(y)
    y = Dense(32, activation='relu')(y)
    metadata_out = Dense(32, activation='relu')(y)

    x = concatenate([cnn_out, metadata_out])

    # We stack a deep densely-connected network on top
    x = Dense(128, activation='relu')(x)
    x = Dropout(0.3)(x)
    x = Dense(256, activation='relu')(x)
    x = Dropout(0.3)(x)
    x = Dense(128, activation='relu')(x)

    # And finally we add the main logistic regression layer
    main_output = Dense(output_size, activation='softmax', name='main_output')(x)
    model = Model(inputs=[image_input, metadata_input], outputs=[main_output])
    return(model)

if __name__ == '__main__':
    nb_features = 6
    output_size = 42
    model = create_model(nb_features, output_size)
    Path('imgs/').mkdir(exist_ok=True)
    plot_model(model,
               to_file=f'imgs/model_f{nb_features}_s{output_size}.png',
               show_shapes=True)
    print(model.summary())
