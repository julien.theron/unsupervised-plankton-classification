"""
This script allows to create clusters of data.
It expects to use use an already pretrained classifier and to use the output
of the second to last layer to transform the input data (image + metadata)
into a 128-D vectors, reduce its dimension and then apply clustering algorithm
on it.

For the current state of the project, only PCA for dimensionnality reduction
and HDBSCAN have been applied.
It is run several time on different value of PCA, to compare results.
Other approach to consider are to use LDA, QDA, NCA or non linear dimension
reduction method and try the OPTICS algorithm
"""

import hdbscan
from joblib import dump, load
from keras.models import load_model, Model
import load_dataset as ld
import train as tr
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from scipy import stats
from sklearn.cluster import KMeans, OPTICS
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import (LinearDiscriminantAnalysis,
                                           QuadraticDiscriminantAnalysis)
from sklearn import metrics
from sklearn.neighbors import (KNeighborsClassifier,
                               NeighborhoodComponentsAnalysis)
from sklearn.preprocessing import normalize, RobustScaler
from sklearn.pipeline import make_pipeline
from sklearn.utils.multiclass import unique_labels
import shutil
import argparse
import pandas as pd
from joblib import dump, load


def get_arg():
    parser = argparse.ArgumentParser()
    default_path = str(Path('..', 'Dataset', 'Kaggle_Current',
                            'Dataset_Plankton'))
    parser.add_argument("-p", "--path", type=str, help='path to dataset',
                        default=default_path)
    parser.add_argument("-c", "--csv", type=str, help='folder containing csv',
                        default='csv')
    args = parser.parse_args()
    print(args)
    return(args.path, args.csv)


def try_clustering(dt, metapath, use_pca=0, cluster_met='eof'):
    X_test, y_test = dt

    # Transform sparce matrix to list of int
    y_test = np.hstack(np.argmax(y_test, axis=1).tolist())

    # Reduce dim  with PCA
    if use_pca:
        print(f'Using PCA to reduce to {use_pca} dimensions')
        pca = make_pipeline(RobustScaler(),
                            PCA(n_components=use_pca,
                            random_state=0))
        pca.fit(X_test)
        X_test = pca.transform(X_test)

    # Use hdscan for clustering

    hdbscan_classifier = hdbscan.HDBSCAN(min_cluster_size=500,
                                         core_dist_n_jobs=-1,
                                         cluster_selection_method=cluster_met
                                         )

    hdbscan_classifier.fit(X_test)
    result = hdbscan_classifier
    score(result.labels_, y_test=None, output=None)
    df = pd.DataFrame({'ImgName': metapath['ImgName'],
                       'cluster': result.labels_})
    df.to_csv(f'csv2/cluster_pca{use_pca}_meth_{cluster_method}.csv')


def score(pred_labels, y_test=None, output=None):
    print(stats.describe(pred_labels))
    unique, counts = np.unique(pred_labels, return_counts=True)
    print(dict(zip(unique, counts)))
    if y_test:
        print('\n Score compared to real category')
        print('adjusted_rand_score')
        print(metrics.adjusted_rand_score(y_test, pred_labels))
        print('homogeneity_score')
        print(metrics.homogeneity_score(y_test, pred_labels))
        print('completeness_score')
        print(metrics.completeness_score(y_test, pred_labels))


def get_new_images(dataset_path, csv_path, personalize):
    metadata_csv = ld.load_csv(csv_path)
    if personalize:
        metadata_csv = metadata_csv[metadata_csv['Category_Grouped'] == personalize]
    # Loading images from the csv
    imgs = ld.load_images(metadata_csv, dataset_path)[:, :, :, np.newaxis]
    # Convert the name of labels into vector + save group
    vector_taxon = metadata_csv['Category_Taxon'].to_numpy()
    labels, class_weights = ld.get_labels(vector_taxon)
    nb_class = labels.shape[1]
    # Drop the feature that are not going to be used in the training
    metapath = metadata_csv[['ImgName', 'Category_Taxon', 'FolderNumber']]
    metadata_csv.drop(['ImgName', 'Category_Taxon', 'Category_Grouped',
                       'NumberOfROIinTrigger', 'FolderNumber'],
                      axis=1, inplace=True)
    metadata_values = metadata_csv.values
    return([imgs, metadata_values], labels, class_weights, nb_class, metapath)


if __name__ == '__main__':
    Path('csv2').mkdir(exist_ok=True)
    dataset_path, csv_path = get_arg()
    inputs, labels, class_weights, nb_class, metapath = \
        get_new_images(dataset_path, csv_path, personalize='Other')
    dependencies = {'top3': tr.top3}
    model = load_model(f'models/my_model_f6_c42_bs_64_e_50.h5',
                       custom_objects=dependencies)
    layer_name = 'dense_9'
    inter_model = Model(inputs=model.input,
                        outputs=model.get_layer(layer_name).output)
    # inter_model = model
    intermediate_output = inter_model.predict(inputs,
                                              verbose=1, batch_size=4096)
    dump(intermediate_output, 'obj/intermediate_output.joblib')
    dump(labels, 'obj/labels.joblib')
    dump(metapath, 'obj/metapath.joblib')
    labels = load('obj/labels.joblib')
    metapath = load('obj/metapath.joblib')
    intermediate_output = load('obj/intermediate_output.joblib')
    dt = [intermediate_output, labels]
    for i in range(1, 50):
        try_clustering(dt, metapath, use_pca=i, cluster_method='eom')
        try_clustering(dt, metapath, use_pca=i, cluster_method='leaf')
