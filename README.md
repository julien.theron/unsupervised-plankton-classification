# Unsupervised classification of plankton'ss images
## Context

This repository is the result of a 2-month internship (August-September 2019) at the University of Maine, in the Marine Science Department.
The dataset used is going to be published on Kaggle, probably before the end of 2019. I'll add it when available.

## Introduction
With the emergence of oceanographic imaging instrument in the past decade, marine scientists have access to an unprecedent number of plankton images. In order to exploit the information hidden in these datasets, automated classifiers are needed as manual taxonomic classification of the images is not realistic. Machine learning algorithms can be an efficient way to answer this problem. For this project, a set of 1.6 M labelled images and associated metadata is used. This dataset is composed of images captured by an imaging cytometer in the western North Atlantic. The images have been put into 97 taxons that are grouped in 17 groups. The size of each taxons varies from hundreds of thousands to less than one hundred. A third of the samples have been put in an “other” group (either by inability to decide in which taxon they belong or because they were not close to any taxons). See [dataset management](###dataset-management) for more information about the dataset.

The objective of the project is to cluster the “other” category in morphologically relevant groups. 
The major steps involved are:

1.	Train a network to classify the labelled cells. This supervised training is done at the taxon level excluding the other category to have the most morphologically homogenous grouping.
2.	The network trained is used to extract relevant features for phytoplankton classification of the other category, using all layers but the last one.
3.	A [Principal Component Analysis (PCA)](https://scikit-learn.org/stable/modules/generated/)  was then applied to reduce the dimensionality of the feature vector.
4.	The reduced feature vector was then clustered using [Hierarchical Density-Based Spatial Clustering of Applications with Noise (HDBSCAN)](https://hdbscan.readthedocs.io/en/latest/index.html).

See [How do this work?](##how-do-this-work?) for more details.

An experimental way to obtain this 'other' dataset has been also has been inspired by this unsupervised learning. See the part [How to classify new data in the other category?](##how-to-classify-new-data-in-the-other-category?) for more details.
    

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

It's recommended to use conda and a 64-bit Linux distribution to generate the environment, even if it should be possible to do it using pip and Windows/MacOS.


### Installing

1. Clone the repository

```
git clone git@gitlab.com:julien.theron/unsupervised-plankton-classification
```

2. Create the new environment. Go to the root of the project and run

```
conda env create -f env/environment.yml
```

### Structure of the project

Script to call:

* create_mosaic.py: Allow to create mosaic of image on folder. Useful for visualizing cluster created by cluster.py. Take 2 arguments : -p for the path to the directory where the cluster are stored and -s for the size of the mosaic in pixel.
* clustering: Use to create cluster and save them into \<nameofthemetod>/\<compressionused>/. Take 2 arguments: -c for the path to the folder with the csv, -p for the path to the images.
* confusion_matrix.py.py: Use to test a model created. Save a confusion matrix in the folder imgs. Take 3 arguments: -s for the number of images in a taxon needed to be keep in the dataset, - p for the path to the images, and -c for the path to the folder with the csv files (metadata.csv and input.csv)
* prediction_csv.py: Use to classify a dataset using a model previously trained. The results are saved in a csv file. Take 2 arguments: -p for the path to the dataset to classify, -c for the path to csv with the metadata.
* random_forest_bin.py: Evaluate the performance of random forest on binary classification and save it in a csv file. See [About the other category](##about-the-other-category), for more information about the metrics. 
* train.py: Code to train the model. Take 3 arguments: -s for the number of images in a taxon needed to be keep in the dataset, - p for the path to the images, and -c for the path to the folder with the csv files (metadata.csv and input.csv)

Script that are not mean to be called on their own:
* create_model.py: Create the keras model for the supervised learning and save an image of it in imgs folder. When called, by train.py dynamically choose the size of the output of the model accordingly to the number of taxon in the dataset after cleaning.
* load_dataset.py: All the function to load the dataset (images+metadata) into memory and drop/remove a cleaning of the data
* README.md: See [README.md](README.md)

Some other scripts in additional-script/. Those where used for plotting graph and other analysis. 
### What do I do now, [Jamy](https://gfycat.com/warmseparategrackle)?

1. Get the dataset to work with. Put it in the folder ../Dataset
2. Train the network. Run train.py
3. Test the network. Run confusion_matrix.py
4. Use the model created to try to cluster data. Run clustering.py
5. Create mosaics to visualize the data. Run create_mosaic.py 

## How do this work?
### Dataset

The dataset used has the advantage to be huge (1.6 million in the version used).
The samples are grouped in 17 groups:
![alt text](imgs_readme/hist_group.png "Histogram of group")
Each sample has also its own taxons:
![alt text](imgs_readme/hist_taxon.png "Histogram of taxons")

The other group has been also divided in several (arbitrary) taxons:
![alt text](imgs_readme/hist_other.png "Histogram other group")

However, there's some issue to manage the dataset:

1) Classes are extremely unbalanced. That's a result of the distribution of the plankton in the ocean. Some taxons have dozens of thousands of samples, when some have less than 200.
In this case, since the work was more to have a network that extract relevant features from images+metadata, rather than to have a classifier that can classify everything, only the taxon with more than 500 samples (42 of them) has been selected. Also, by lack of time no data augmentation has been done (kind of complicated since both the image and metadata need to be augmented), but some class_weight has been added during the training with Keras to give more weight to under-represented taxon. In addition to the Other taxon, BQL, Not-living, Multiple, Clumps and Bad-Focus taxons have been excluded of the training. BQL has been excluded since it's a category artificially created for cell that are smaller than a defined area and therefor are only determined by one of the metadata. Not-living, Multiple, Clumps and Bad-Focus because those are special taxon that do not represent normal cells.
2) Images have various size. In order to fit the network all images have to be resize to a fixed size. The size has been fixed using the medium x and y size. The choice that has been made is simply to resize to fit the image with no intend to keep the original proportion, and simply use the original as features to train the network.
3) Metadata have some impossible values. It can be some area that are equal to zero or way to big to be valid data, or even some volume that are negative. Per se it's not a big deal that some value are equal to zero of slightly negative, if the "real" value is actually close to the impossible value (since the value are going to be scaled anyway). A robust scaler has been used to avoid 'bad' scaling. A work with the input.csv has been started to remove those impossible value from the training dataset but is not currently used.

### Neuronal network
The neuronal network is divided in 3 parts:
1) A convolutional network whose mission is to extract features from the images. 
2) A fully connected network to combinate metadata
3) A fully connected network to combinate the outputs of the two other parts 

The CNN has a structure inspired by VGG network with Batch normalisation and dropout layers. See the code for more information.

### Example of trained model

Here an example of confusion matrix for a model trained:
![alt text](imgs_readme/confusion_matrix_f6_c42.jpg "Confusion matrix")

Results are pretty good when trained only the taxon that have more than 500 samples.

### Use the model to classify

After having trained a model that has a good precision on known taxons, it can be used as a feature extractor on the other category. The second to last layer has been used as a feature extractor of the other images. Then the dimension of features has been reduced to 10 using PCA and HDBSCAN has been used to 

Here are some of the best results achieved:
![alt text](imgs_readme/7_6.png "Cluster number 7")
![alt text](imgs_readme/8_7.png "Cluster number 8")
![alt text](imgs_readme/9_2.png "Cluster number 9")
![alt text](imgs_readme/11_8.png "Cluster number 11")

The “other” category was clustered into 15 groups of morphologically similar cells, and some of which have known taxonomy. A qualitative evaluation of the cluster shows that some of the clusters are redundant and a few are heterogenous. Half of the data passed to the clustering algorithm HDBSCAN was considered as noise and excluded from those clusters.
Future work to improve the clustering includes: the exploration of other clustering parameters used with HDBSCAN, testing other dimensionality reduction algorithm (e.g. ([LDA](https://scikit-learn.org/0.16/modules/generated/sklearn.lda.LDA.html#sklearn.lda.LDA), [QDA](https://scikit-learn.org/0.16/modules/generated/sklearn.qda.QDA.html), [NCA](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.NeighborhoodComponentsAnalysis.html)), or testing other clustering algorithm (e.g. [OPTICS](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.OPTICS.html)).

## How to classify new data in the other category?
### Classical Approach
A classical way to obtain this other category from new unclassified data could be to have a trained network on all taxonomy including the other category. However, this method could have a hard time to perform correctly. Indeed the other in not a 'real' category on its own, more of a category for samples that don't really belong to any other category, and thus is not an homogenous category with fixed features. Worst, on a new data there might be some new cells/scrap that haven't be seen in the training dataset. Thus it's hard to predict if the model will be able to determine that it should be put in other category (because it doesn't look like anything known), or if it will be put in a the closest taxon by default.

### Approach proposed
In order to tackle this issue, another approach has been chosen: doing a classification without having an explicit other category. After have forced every sample in a category, we clean those with a binary classification to separate true and false positive. False positive removed are then put in the other category. This binary classification is done on the second to last layer's output of the model.

### Can we separate true and false positive?
To ensure that the binary classification make sense, some visualisation can be done, after projecting the 128D vector into a 2D space using PCA. For a better understanding of those results it's important to keep in mind that usually true positive and false positive are not present in the same proportion in one category.
![alt text](imgs_readme/Acanthoica.png "Acanthoica")
![alt text](imgs_readme/Bacillariophyta-centric.png "Bacillariophyta-centric")
![alt text](imgs_readme/Chaetoceros.png "Chaetoceros")
![alt text](imgs_readme/Cryptophyceae-like.png "Cryptophyceae-like")
![alt text](imgs_readme/Dictyochales.png "Dictyochales")
![alt text](imgs_readme/Dinophyceae.png "Dinophyceae")
![alt text](imgs_readme/Prorocentrum.png "Prorocentrum")

For a great part of those category it's easy to do a separation that would greatly improve the 'purity' of the category, even while being in 2D.

### Random forest and metrics

Since visualisation confirmed that there's good hope that precision and recall can be improved by trying to separate true and false positive, some binary classification has been done using random forest.

10 metrics are calculated:
1) BaseNb: The number of elements in the Taxonomy
2) BaseNbTP: The number of elements in the Taxonomy that actually belong to the taxonomy
3) RfNbP: Positive element for the random forest
4) RfNbTP: True positive for the random forest
5) RfNbN: Negative element for the random forest
6) RfNbTN: True negative for the random forest
7) BPTP: Precision (Proportion of true positive) before the random forest
8) RfPTP: Precision after the random forest for the positive
9) RfPTN: Precision after the random forest for the negative
10) RfPropTPKeep: Recall after the random forest for the positive

The latest 4 metrics are particularly interesting. Comparing 7 and 8 allow to know if the random forest made the dataset cleaner. 9 allow to see what proportion of the negative for the random forest is actually negative. 10 allow to see how many of the true positive remains in the dataset.



|Category_Taxon               |BaseNb|BaseNbTP|RfNbP|RfNbTP|RfNbN|RfNbTN|BPTP |RfPTP|RfPTN|RfPropTPKeep     |
|-----------------------------|------|--------|-----|------|-----|------|-----|-----|-----|-----------------|
|Acanthoica                   |67094 |1168    |917  |613   |66177|65622 |0.017|0.668|0.992|0.524828767123288|
|Bacillariophyta-centric      |39310 |8407    |7410 |5386  |31900|28879 |0.214|0.727|0.905|0.640656595694064|
|Bacillariophyta-centric-chain|5799  |3136    |3143 |2632  |2656 |2152  |0.541|0.837|0.81 |0.839285714285714|
|Bacillariophyta-pennate      |13777 |6677    |6603 |5546  |7174 |6043  |0.485|0.84 |0.842|0.830612550546653|
|Bacillariophyta-pennate-chain|1552  |1206    |1484 |1168  |68   |30    |0.777|0.787|0.441|0.96849087893864 |
|Bubble                       |338   |216     |214  |210   |124  |118   |0.638|0.981|0.952|0.972222222222222|
|Ceratium                     |227   |220     |221  |217   |6    |3     |0.971|0.982|0.5  |0.986363636363636|
|Chaetoceros                  |99634 |30493   |30162|28274 |69472|67253 |0.306|0.937|0.968|0.927229200144295|
|Ciliophora                   |2686  |914     |831  |708   |1855 |1649  |0.34 |0.852|0.889|0.774617067833698|
|Cocco-cone                   |392   |136     |121  |101   |271  |236   |0.347|0.835|0.871|0.742647058823529|
|Corethron                    |2061  |767     |740  |693   |1321 |1247  |0.372|0.936|0.944|0.903520208604954|
|Cryptophyceae-like           |16186 |11331   |12256|10506 |3930 |3105  |0.7  |0.857|0.79 |0.92719089224252 |
|Cylindrotheca                |601   |344     |400  |323   |201  |180   |0.572|0.808|0.896|0.938953488372093|
|Dictyochales                 |4624  |3421    |3410 |3279  |1214 |1072  |0.74 |0.962|0.883|0.958491669102602|
|Dinophyceae                  |115381|26209   |22445|17267 |92936|83994 |0.227|0.769|0.904|0.658819489488344|
|Ditylum                      |175   |170     |173  |170   |2    |2     |0.974|0.983|1    |1                |
|Euglenida                    |14757 |13629   |14353|13476 |404  |251   |0.924|0.939|0.621|0.98877393792648 |
|Flagellates-like             |61922 |2248    |208  |92    |61714|59558 |0.036|0.442|0.965|0.040925266903915|
|Guinardia                    |1004  |764     |828  |717   |176  |129   |0.761|0.866|0.733|0.93848167539267 |
|Guinardia-delicatula         |199   |162     |192  |159   |7    |4     |0.815|0.828|0.571|0.981481481481482|
|Karenia                      |325   |126     |116  |101   |209  |184   |0.387|0.871|0.88 |0.801587301587302|
|Membraneis                   |1002  |848     |895  |827   |107  |86    |0.847|0.924|0.804|0.975235849056604|
|Nano-Diatom                  |78    |12      |3    |1     |75   |64    |0.154|0.333|0.853|0.083333333333333|
|Nanoneis                     |204   |182     |188  |180   |16   |14    |0.894|0.957|0.875|0.989010989010989|
|Nitzschia                    |146   |123     |132  |118   |14   |9     |0.843|0.894|0.643|0.959349593495935|
|Oxytoxum                     |5953  |4821    |5100 |4600  |853  |632   |0.81 |0.902|0.741|0.954158888197469|
|Phaeocystis                  |2845  |206     |155  |123   |2690 |2607  |0.073|0.794|0.969|0.597087378640777|
|Prorocentrum                 |8125  |2838    |2797 |2415  |5328 |4905  |0.349|0.863|0.921|0.850951374207188|
|Prymnesiaceae                |2538  |1350    |1362 |1235  |1176 |1061  |0.532|0.907|0.902|0.914814814814815|
|Prymnesio-like               |1400  |515     |549  |382   |851  |718   |0.368|0.696|0.844|0.741747572815534|
|Prymnesiophyceae             |11920 |437     |17   |11    |11903|11477 |0.037|0.647|0.964|0.025171624713959|
|Pseudo-nitzschia             |2571  |1405    |1344 |941   |1227 |763   |0.546|0.7  |0.622|0.669750889679715|
|Pterosperma                  |1446  |197     |179  |152   |1267 |1222  |0.137|0.849|0.964|0.771573604060914|
|Pyramimonas                  |799   |340     |333  |271   |466  |397   |0.426|0.814|0.852|0.797058823529412|
|Rhabdosphaeraceae            |278   |265     |269  |264   |9    |8     |0.952|0.981|0.889|0.99622641509434 |
|Rhizosolenia                 |155   |134     |143  |126   |12   |4     |0.862|0.881|0.333|0.940298507462687|
|Syracosphaera                |1024  |436     |463  |375   |561  |500   |0.426|0.81 |0.891|0.860091743119266|
|Syracosphaerales             |420   |223     |212  |194   |208  |179   |0.531|0.915|0.861|0.869955156950673|
|T12-Prymnesio                |1824  |215     |108  |99    |1716 |1600  |0.118|0.917|0.932|0.46046511627907 |
|Thalassionema                |57    |54      |56   |54    |1    |1     |0.941|0.964|1    |1                |
|Thalassiosira                |661   |540     |567  |529   |94   |83    |0.817|0.933|0.883|0.97962962962963 |
|Torodinium                   |422   |358     |375  |344   |47   |33    |0.849|0.917|0.702|0.960893854748603|


Some stats about the results:
1) The precision (BPTP) is 54% before the random forest and 84% after (median value 54% and 86%).
2) The precision for the negative (RfPTN) is 83% (median value 88%)
3) The recall (RfPropTPKeep) is 80% (median value 91%)

Those result prove that using a random forest binary classifier on top of a classifier is a real improvement over just the network. However in order to confirm that those result are interesting we need to compare those results to the more classical approach (with a network that consider the other category as a 'normal' category) 

## TODO LIST

Since the whole process is divided in 2 depend steps (training a model and then using clustering on intermediate outputs of the model) the improvements can be managed on both of those side:
- Improvement of the quality of features: This can be managed be having a network that work better and to choose the right part of the network to extract the features. Ways to manage that includes (but are not limited to): having a better structure for the network, a better pre-processing of images and cleaning the csv,...
- Improvement of the clustering: Using other dimensionality method than PCA, maybe nonlinear one. Trying other methods of clustering than HDBSCAN (OPTICS,...)

## Additional resources:

You can access additional resource  at https://drive.google.com/drive/folders/1kSsvr_7MJuVLmkxQyTB7GgpCnHq3Cm6B

This repository includes:
* 1 pre-trained model with 42 classes (taxon with a size of 500 or more)
* 1 input.csv file: Define the rank of acceptable values (for metadata)
* 1 labels_unique file. Allow to create the confusion matrix (find association between vector label used by the machine learning algorithm and their names)

## Built With

* [Anaconda](https://www.anaconda.com/) - Distribution of Python for scientific computing
* [Keras](https://keras.io) - Neural network library written in Python
* [Scikit-learn](https://scikit-learn.org/stable/) - Machine library for Python
* [pandas](https://pandas.pydata.org/) - Python Data Analysis Library

## Authors

* **Julien Theron** - *Main author* - [J.Theron](https://gitlab.com/julien.theron)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspiration from [Emmett](https://github.com/emmettFC)'s model for the supervised part









